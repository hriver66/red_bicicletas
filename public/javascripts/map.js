var map = L.map('main_map').setView([-34.630300, -58.409618], 13);


L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?{foo}',
 {foo: 'bar', attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'}).addTo(map);

/*
var marker = L.marker([-34.632300, -58.418970]).addTo(map);
var marker = L.marker([-34.634300, -58.409860]).addTo(map);

//marker.bindPopup("<b>Aqui Toy!</b><br>").openPopup();
*/

$.ajax({
    dataType: 'json',
    url: 'api/bicicletas',
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion,{title: bici.id}).addTo(map);
        });
    },
});

