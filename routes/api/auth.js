const express = require('express');
const router = express.Router();
const passport = require('../../config/passport');

const auhtController = require('../../controllers/api/authControllerAPI');

//const { authenticate } = require('passport');
//const authControllerAPI = require('../../controllers/api/authControllerAPI');

router.post('/authenticate', auhtController.authenticate);
router.post('/forgotPassword', auhtController.forgotPassword);
router.post('/facebook_token', passport.authenticate('facebook-token'), auhtController.authFacebookToken);

module.exports = router
