var Bicicleta = require('../../models/bicicleta');


exports.bicicleta_list = function (req, res){
    Bicicleta.allBicis(function(err, bicis){
        res.status(200).json({bicicletas:bicis});
    });
}


exports.bicicleta_create = function(req, res){
    // pasar el json o body completo al schema Bicicleta
    var bici = new Bicicleta(req.body);
    bici.ubicacion=[req.body.lat, req.body.lng];

    //console.log(req.body);

    Bicicleta.add(bici, function(err, newBici){
        res.status(200).json({bicicleta:newBici});
    });
}


// pendientes por revisar ajustar

exports.bicicleta_update = function(req, res){
    Bicicleta.findByCode(req.body.id,function(err,bici){
        bici.color=req.body.color;
        bici.modelo= req.body.modelo;
        bici.ubicacion=[req.body.lat, req.body.lng];
        res.status(200).json({bicicleta: bici});
    });    
}


exports.bicicleta_delete = function(req, res){
    Bicicleta.removeByCode(req.body.id, function(err,bici){ 
        res.status(204).send();
    });
}

