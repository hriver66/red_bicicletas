var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function (req, res){
    Bicicleta.allBicis(function(err, bicis){
        res.render('bicicletas/index',{bicis:bicis});
    });
}

exports.bicicleta_create_get = function(req, res){
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = function(req, res){
    var bici = Bicicleta.createInstance(req.body.code, req.body.color, req.body.modelo, [req.body.lat, req.body.lng]);
    Bicicleta.add(bici, function(err, newBici){
        res.redirect('/bicicletas');
    });
}


exports.bicicleta_update_get = function(req, res) {

    Bicicleta.findByCode(req.params.id,function(err,bici) {
        res.render('bicicletas/update', { bici: bici });
    });
};

exports.bicicleta_update_post = function(req, res) {
    var bici = {
        code: req.body.code,
        color: req.body.color,
        modelo: req.body.modelo,
    }
    bici.ubicacion=[req.body.lat, req.body.lng];
    
    // Update bici
    Bicicleta.update(bici,function() {
        // Redirect to List page
        res.redirect('/bicicletas');
    });
};

exports.bicicleta_delete_post = function(req,res){
    Bicicleta.removeByCode(req.params.id,function(err, bici){
        res.redirect('/bicicletas');
    });

}