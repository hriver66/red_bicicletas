var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas',function(){
    // se conecta antes de cada test
    beforeEach(function(done){
        var mongoDB= 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB,{useNewUrlParser: true, useUnifiedTopology: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console,'MongoDB connection error: '));
        db.once('open',function(){
            console.log('we are connected to test database');
            done();
        });
    });


    // elimina todo de la colecciòn despues de cada test  
    afterEach(function(done){
        Bicicleta.deleteMany({},function(err,success){
            if(err) console.log(err);
            mongoose.disconnect(err); 
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('Crea una instancia de Bicicleta',() => {
            var bici = Bicicleta.createInstance(1,"Verde","Urbana",[-34.5,-54.1]);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("Verde");
            expect(bici.modelo).toBe("Urbana");
            expect(bici.ubicacion[0]).toBe(-34.5);
            expect(bici.ubicacion[1]).toBe(-54.1);
            
        });
        
    });

    describe('Bicicleta.allBicis', () => {
        it('Comienza Vacia', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            });
            
        });
    });

    describe('Bicicleta.add', () => {
        it('Agrega solo una bici', (done) => {
            var aBici = new Bicicleta({code: 1, color: "Verde", modelo: "Urbana"});
            Bicicleta.add(aBici, function(err, newBici){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();
                })
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('Debe devolver la bici con code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
         
                var aBici = new Bicicleta({code: 1, color: "Verde", modelo: "Urbana"});
                Bicicleta.add(aBici, function(err, newBici){
                    if(err) console.log(err);

                    var aBici2 = new Bicicleta({code: 2, color: "Negra", modelo: "Montana"});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if(err) console.log(err);

                        Bicicleta.findByCode(1,function(error,targetBici){
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);
                            done();        
                        });
                    });        
                    
                });
            });
        });
    });

});

