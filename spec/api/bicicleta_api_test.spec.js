var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var server = require('../../bin/www');
var request = require('request');

var base_url ="http://localhost:3000/api/bicicletas";

describe('Bicicleta API', () => {
    
    beforeAll((done) => { mongoose.connection.close(done) });

    beforeEach(function(done){
        mongoose.disconnect(); 
        
        var mongoDB= 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB,{useNewUrlParser: true, useUnifiedTopology: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console,'MongoDB connection error: '));
        db.once('open', function() {
            console.log('we are connected to test database');
            done();
        });
       
    });

    
    // elimina todo de la colecciòn despues de cada test  
    afterEach(function(done){
        Bicicleta.deleteMany({},function(err,success){
            if(err) console.log(err);
            mongoose.disconnect(err); 
            done();    
        });
      
    });

    describe('GET Bicicletas /', () => {
        it('status 200', (done) => {
            request.get(base_url, function(error, response, body){
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                console.log('*** ' + body);
                done();
            });
        });
    });


    describe('POST BICICLETAS /create', () => {
        // done llama el callback
        it('STATUS 200', (done) => {
            var headers={'Content-type': 'application/json'};
            var aBici = '{"code":10, "color": "Rojo", "modelo": "Montaña","lat":-34,"lng":-58}';
   
            request.post({
                headers : headers,
                url: base_url+'/create',
                body: aBici
            },  function(error, response, body){
                    expect(response.statusCode).toBe(200);
                    var bici = JSON.parse(body).bicicleta; 
                    console.log(bici);
                    expect(bici.color).toBe("Rojo");
                    expect(bici.ubicacion[0]).toBe(-34);
                    expect(bici.ubicacion[1]).toBe(-58);
                    done();
            });    
        });
    });
    

    /*
    describe('POST BICICLETAS /update', () => {
        // done llama el callback
        it('STATUS 200', (done) => {
            expect(Bicicleta.allBicis.length).toBe(0);
       
            //datos de prueba
            var a = new Bicicleta(1,'Rojo','Urbana',[-34.630300, -58.409618]);
            Bicicleta.add(a);
          
            var headers={'Content-type': 'application/json'};
            var aBici = '{"id":1,"color":"Azul","modelo":"Hibrida","lat":-34.630300,"lng":-58.409618}'
        
            request.put({
                headers : headers,
                url: base_url+"/update",
                body: aBici
            },  function(error, response, body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(1).color).toBe("Azul");
                expect(Bicicleta.findById(1).modelo).toBe("Hibrida");
                expect(Bicicleta.allBicis.length).toBe(1);
                done();
            });
        });
    });

   

    describe('POST BICICLETAS /delete', () => {
        // done llama el callback
        it('STATUS 204', (done) => {
            //datos de prueba
            var a = Bicicleta.createInstance(1,'Rojo','Urbana',[-34.630300, -58.409618]);
            Bicicleta.add(a, function(err,newBici){
                var headers={'Content-type': 'application/json'};
                var aBici = '{"id":1}';
              
                request.delete({
                    headers : headers,
                    url: base_url+'/delete',
                    body: aBici
                },  function(error, response, body){
                    expect(response.statusCode).toBe(204);
                    expect(a.findByCode(1).color).toBe("Rojo");
                    expect(a.length).toBe(1);
                    done();
                });

            });
            

        });
    
    });

   */

    
});


