// Use at least Nodemailer v4.1.0
const nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');

let mailConfig;

if (process.env.NODE_ENV==='production'){
    const options = {
        auth:{
            api_key: process.env.SENDGRID_API_SECRET
        }
    }
    mailConfig = sgTransport(options);

}else{
    // AMB PRUEBAS
    if (process.env.NODE_ENV==='staging'){
        console.log('XXXXXXXXXXXXXXX');
        const options = {
            auth:{
                api_key: process.env.SENDGRID_API_SECRET
            }   
        }
        mailConfig = sgTransport(options);

    }else{

        // AMB DESARROLLO
        // emails catched by ethereal.email    
        mailConfig = {
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
               user: process.env.ethereal_user,
               pass: process.env.ethereal_pwd
            }     
        };    
    }
}


module.exports = nodemailer.createTransport(mailConfig);
